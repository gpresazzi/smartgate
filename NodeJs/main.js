/*jslint node:true, vars:true, bitwise:true, unparam:true */
/*jshint unused:true */
/*global */

/*
A simple node.js application intended to blink the onboard LED on the Intel based development boards such as the Intel(R) Galileo and Edison with Arduino breakout board.

MRAA - Low Level Skeleton Library for Communication on GNU/Linux platforms
Library in C/C++ to interface with Galileo & other Intel platforms, in a structured and sane API with port nanmes/numbering that match boards & with bindings to javascript & python.

Steps for installing MRAA & UPM Library on Intel IoT Platform with IoTDevKit Linux* image
Using a ssh client: 
1. echo "src maa-upm http://iotdk.intel.com/repos/1.1/intelgalactic" > /etc/opkg/intel-iotdk.conf
2. opkg update
3. opkg upgrade

Article: https://software.intel.com/en-us/html5/articles/intel-xdk-iot-edition-nodejs-templates
*/

var mraa = require('mraa'); //require mraa
var LCD  = require ('jsupm_i2clcd');
console.log('MRAA Version: ' + mraa.getVersion()); //write the mraa version to the Intel XDK console

//var myOnboardLed = new mraa.Gpio(3, false, true); //LED hooked up to digital pin (or built in pin on Galileo Gen1)
var myOnboardLed = new mraa.Gpio(4); //LED hooked up to digital pin 13 (or built in pin on Galileo Gen2)
myOnboardLed.dir(mraa.DIR_OUT); //set the gpio direction to output
var ledState = true; //Boolean to hold the state of Led


var express = require('express')
var app = express()
var light = new mraa.Aio(0);
var inkBoard = new mraa.Aio(1);
var lightValue;
var firstRow="";
var secondRow = "";
var myLCD = new LCD.Jhd1313m1(2, 0x3E, 0x62);
var host;
var port;

///////////////READ ADDRESS /////////////////////////////
var os = require('os');
var ifaces = os.networkInterfaces();

Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0
    ;

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      // this single interface has multiple ipv4 addresses
      console.log(ifname + ':' + alias, iface.address);
    } else {
      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);
      host = iface.address;
    }
  });
});

/////////////// LOOP FOR LCD AND INK/////////////////////////////
loop();
var inkValue;
var openingTimeInk = false;
function loop(){
    lightValue  = light.read();
    lightValue = Math.round( lightValue*.1);
    //lcdMessage = "Light is @ "+lightValue+"%";
    if(firstRow != "")
    {
        myLCD.setCursor(0,1);
        myLCD.write(firstRow);
    }
    
    if(secondRow != "")
    {
        myLCD.setCursor(1,1);
        myLCD.write(secondRow);
    }
    
    inkValue = inkBoard.read();
    console.log("ink value:" + inkValue);
    if(inkValue > 500)
    {
        if(openingTimeInk == false)
        {
            //opening
            opening = true;

            setTimeout(function() {
                closing = true;
                openingTimeInk = false;
            }, 5000);
            openingTimeInk = true;
        }
        
    }
    
    setTimeout(loop, 500);
}


///////////////////////////// WEB SERVER /////////////////////////////////
app.get('/', function (req, res) {
  //res.send('Hello World!');
  setHttpPage(res);
    periodicActivity(); //call the periodicActivity function

    console.log('get request');
});

/////////////////////////////// SERVO //////////////////////////////////////
//Initialize PWM on Digital Pin #3 (D3) and enable the pwm pin
var pwm3 = new mraa.Pwm(3);
var pwm6 = new mraa.Pwm(6);
var pwm5 = new mraa.Pwm(5);

pwm3.enable(true);
pwm6.enable(true);
pwm5.enable(true);


//set the period in microseconds.
pwm3.period_us(20000);
pwm6.period_us(20000);

pwm5.period_us(2000); //1ms


var maxOpen = 1800 / 20000;
var maxClose = 800 / 20000;

var valueP3 = maxClose;
var valueP6 = maxOpen;
var pwmLed = 1;
pwm5.write(pwmLed); //Write duty cycle value.

var opening = false;
var closing = false;

setInterval(move, 25);
function move() {

    if(opening == true)
    {
        if (valueP3 < maxOpen){
            valueP3 = valueP3 + (20/20000);
            if(valueP6 > maxClose)
                valueP6 = valueP6 - (20/20000);
            
            pwmLed = pwmLed - 0.02;
            pwm5.write(pwmLed); //Write duty cycle value.
            periodicActivity();
        }else{
            opening = false;
            setTimeout(opened, 100);
        }
    }
    
    if(closing == true)
    {
        if (valueP3 > (800/20000)){
            valueP3 = valueP3 - (20/20000);
            if(valueP6 < maxOpen)
                valueP6 = valueP6 + (20/20000);
            
            pwmLed = pwmLed + 0.02;
            pwm5.write(pwmLed); //Write duty cycle value.
            periodicActivity();
        }else{
            closing = false;
            setTimeout(closed, 100);
        }
    }
    pwm3.write(valueP3); //Write duty cycle value. 
    pwm6.write(valueP6); //Write duty cycle value.
    
    
    
    //console.log(pwm3.read());//read current value that is set before.
}


/////////////////////////// HTTP REQUESTS ///////////////////
app.get('/display', function (req, res) {
  secondRow = " -- DISPALY --";
  //res.send('Display!');
  setHttpPage(res);
  console.log('get request');
});

app.get('/open', function (req, res) {
  secondRow = " -- OPENING --";
  //setTimeout(open, 1000);
  //res.send('Opening ---');
  opening = true;
  setHttpPage(res);
  console.log('get request');
});

app.get('/close', function (req, res) {
  secondRow = " -- CLOSING --";
  //setTimeout(close, 1000);
  //res.send('Opening ---');
  closing = true;
  setHttpPage(res);
  console.log('get request');
});

var server = app.listen(8080, function () {
    
  //host = server.address().address
  port = server.address().port
  firstRow = "" + host + ":" + port + "    ";
    
  console.log('Example app listening at http://%s:%s', host, port)

});

function setHttpPage(res){
    res.send("<a href='./open'>Open</a><br><a href='./close'>Close</a><br>");
}

function periodicActivity(){
    console.log('Change led state: ' + ledState);
    myOnboardLed.write(ledState?1:0); //if ledState is true then write a '1' (high) otherwise write a '0' (low)
    ledState = !ledState; //invert the ledState
  //setTimeout(periodicActivity,1000); //call the indicated function after 1 second (1000 milliseconds)
}

var dgram = require('dgram');
var client = dgram.createSocket('udp4');
// UDP Options
var options = {
    host : '127.0.0.1',
    port : 41234
};

//////////////////////// OBJECT FOR IOT //////////////////
var item = {  sensorName : "gatecounter", sensorType: "gatecounter.v1.0"};

//register device
registerNewSensor(item.sensorName, item.sensorType, function () {});  
var accessCounter = 0;

function opened(){
    secondRow = " -- OPENED --";
    
    accessCounter++;
    writeCounter(accessCounter);
    sendObservation(item.sensorName, accessCounter, new Date().getTime());
}

function closed()
{
    secondRow = " -- CLOSED --";
}

////////////////////////////////// FILE SYSTEM ///////////////////
var fs = require('fs');

function writeCounter(counter)
{
    fs.writeFile("/tmp/accesscounter", accessCounter + "", function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    }); 
}

function readCounter()
{
    fs.readFile('/tmp/accesscounter', 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      console.log(data); 
      accessCounter = data;
    });
};
readCounter();
////////////////////////////// IOT ANALYTICS //////////////////////////
function registerNewSensor(name, type, callback){
    var msg = JSON.stringify({
        n: name,
        t: type
    });

    var sentMsg = new Buffer(msg);
    console.log("Registering sensor: " + sentMsg);
    client.send(sentMsg, 0, sentMsg.length, options.port, options.host, callback);
};

function sendObservation(name, value, on){
    var msg = JSON.stringify({
        n: name,
        v: value,
        on: on
    });

    var sentMsg = new Buffer(msg);
    console.log("Sending observation: " + sentMsg);
    client.send(sentMsg, 0, sentMsg.length, options.port, options.host);
};

