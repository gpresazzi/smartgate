package com.example.bluetoothgatt;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.TimerTask;

public class HttpCommandSender extends TimerTask {

	private String my_cmd = null;
	
	public static final String ip = "10.2.2.95";
	
	public HttpCommandSender(String cmd)
	{
		my_cmd = cmd;
	}
	
	public void run() {
		
		String urlStr = "http://" + ip + ":8080/" + my_cmd;
		URL url;
		try {
			url = new URL(urlStr);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			InputStream is = connection.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
			}
	
}
